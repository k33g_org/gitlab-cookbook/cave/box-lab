FROM gitpod/workspace-full

USER gitpod

RUN brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey && \
    brew install qemu

