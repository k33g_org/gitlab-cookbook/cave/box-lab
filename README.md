# box-lab

- https://dev.to/mixartemev/minimalistic-way-to-create-cloud-image-from-iso-with-qemu-4lom
- https://alpinelinux.org/downloads/

curl -LJO "https://dl-cdn.alpinelinux.org/alpine/v3.14/releases/x86_64/alpine-standard-3.14.2-x86_64.iso"

qemu-img create -f qcow2 raw_hdd_10.qcow2 10G
qemu-system-x86_64 -boot d -cdrom ./alpine-standard-3.14.2-x86_64.iso -hda raw_hdd_10.qcow2

sudo qemu-system-x86_64 -enable-kvm -cdrom http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/mini.iso

sudo qemu-system-x86_64 -enable-kvm -cdrom 


curl -LJO https://github.com/rancher/k3os/releases/download/v0.22.2-k3s2r0/k3os-amd64.iso

curl -LJO http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/mini.iso

qemu-img create -f qcow2 raw_hdd_10.qcow2 10G
qemu-system-x86_64 -boot d -cdrom ./mini.iso -hda raw_hdd_10.qcow2 \
-net user,hostfwd=tcp::10022-:22 -net nic -nographic



ssh rancher@localhost -p10022


kvm -m 2048 -smp 2 -hda ubuntu-18.10-server-cloudimg-amd64.img -hdb user-data.img -net nic -net user,hostfwd=tcp::1810-:22 -nographic

Then you can ssh into the machine w/ ssh ubuntu@localhost -p1810.